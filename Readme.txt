Readme

update:
    cache service added to cache results
    cache handler could be set in parameters.yml
    (both redis and memcached are usable ( for using memcached php-memcached must be installed))

how to use

clone project 

composer install

create your database

set DataBase Parameters in parameters.yml

at the root of project run command vendor/bin/doctrine orm:schema-tool:update --force

run redis (host:localhost ,port:6379)

run elastic search(host:localhost,port:9200)

run server using php (php -S 127.0.0.1:8000 -t public)

open browser go to 127.0.0.1:8000/create-index to set mapping and index in elastic search(done will be shown)

go to http://127.0.0.1:8000/user/auth    and register 

admin panel product list address http://127.0.0.1:8000/admin/product/index

create a product 

and after going to edit page click yellow button to add new variant color and price(no js validation price must be integer)  

after adding variant click update to save variant

and at the home page you can see the new products and search them

routing file exists in app/config/routing.yml



future enhancement 
	parameters for redis connection must be dynamic in parametrs.yml 
	better exceptionHandler must be added
	dependency injections could go to seperate yaml file
	js validation must be added
	real pagination must be added
	indexing must also be add after post load of entities( now we only do it in creating,updating,deleting)








