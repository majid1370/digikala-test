<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Yaml;

require_once "vendor/autoload.php";
require_once "app/config/parameters.php";

$parameters = Yaml::parse(file_get_contents(__DIR__.'/app/config/parameters.yml'));

$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src"), $isDevMode);
$conn = $parameters['database']; 
$entityManager = EntityManager::create($conn, $config);