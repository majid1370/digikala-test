<?php

use MainBundle\Listener\BackendListener;
use MainBundle\Listener\ExceptionListener;
use MainBundle\Resolver\myControllerResolver;
use ProductBundle\Listener\ProductChangeSubscriber;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Yaml;
use Predis\Client;


class Kernel
{
    public $containerBuilder;
    public $dispatcher;
    public $routeCollection;
    public function __construct()
    {
        $this->containerBuilder = new ContainerBuilder();
        $this->routeCollection = new RouteCollection();
        $this->dispatcher = new EventDispatcher();
    }
    public function getEntityManager()
    {
        $parameters = Yaml::parse(file_get_contents(__DIR__.'/config/parameters.yml'));
        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."../src"), $isDevMode);
        $conn = $parameters['database'];
        $entityManager = EntityManager::create($conn, $config);

        return $entityManager;
    }

    public function setEvents()
    {
        $matcher = new UrlMatcher($this->routeCollection, new RequestContext());
        $this->dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));
        $entityManager = $this->getEntityManager();
        $listener = new BackendListener($entityManager);
        $exceptionListener = new ExceptionListener();
        $this->dispatcher->addListener(KernelEvents::REQUEST,array($listener,'handle'));
        $this->dispatcher->addListener(KernelEvents::EXCEPTION,array($exceptionListener,'handle'));
        $productChangeSubscriber = new ProductChangeSubscriber($this->containerBuilder->get('elastic.search'));
        $this->dispatcher->addSubscriber($productChangeSubscriber);
    }

    public function setContainer()
    {
        $parameters = Yaml::parse(file_get_contents(__DIR__.'/config/parameters.yml'));
        $entityManager = $this->getEntityManager();
        $this->containerBuilder->register('db.manager', 'MainBundle\Services\DBManager')
                                ->addArgument($entityManager);

        // todo find better place to put this !!!!!!
            $twig = new \Twig_Environment(new \Twig_Loader_Filesystem([
                '../src/BackendBundle/Resources/views',
                '../src/FrontendBundle/Resources/views',
            ]));

        $this->containerBuilder->register('template','MainBundle\Services\Template')
                                ->addArgument($twig);

        $elasticSearchConfig = $parameters['elastic-search'];
        $client = Elasticsearch\ClientBuilder::create()->setHosts($elasticSearchConfig)->build();
        $this->containerBuilder->register('elastic.search','MainBundle\Services\ElasticSearch')
                                ->addArgument($client);

        $this->containerBuilder->register('digi.test.authentication','UserBundle\Services\UserAuthenticationService')
                                ->addArgument($entityManager);
        
        $this->containerBuilder->register('my.event.dispatcher','MainBundle\Services\MyEventDispatcher')
                                ->addArgument($this->dispatcher);

        $cacheParameters = $parameters['cache'];
        $cacheExpire = $parameters['expire'];

        if ($cacheParameters['handler'] == "redis"){
            $redisParameters = $parameters['redis'];
            $client = new Client(['host' =>$redisParameters['host'],'port'=>$redisParameters['port']]);
//            $client->
        }elseif ($cacheParameters['handler'] == "memcached"){
            $memcachedParameters = $parameters['memcached'];
            $client = new \Memcached();
            $client->addServer($memcachedParameters['host'], $memcachedParameters['port']);
        }else{
            throw new Exception('no valid cache handler');
        }
        $this->containerBuilder->register('cache.manager','MainBundle\Services\CacheManager')
                                ->addArgument($client,$cacheExpire);

        return $this->containerBuilder;

    }

    public function setRouteCollection()
    {
        $routes = Yaml::parse(file_get_contents(__DIR__.'/config/routing.yml'));
        foreach ($routes as $key => $route){
            $this->routeCollection->add($key,new Route($route['path'],['_controller' => $route['controller']]));
        }
    }

    public function getHttpKernel()
    {
        $controllerResolver = new myControllerResolver($this->containerBuilder);
        $argumentResolver = new ArgumentResolver();
        return new HttpKernel($this->dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

    }
}