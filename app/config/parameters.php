<?php

$parameters = [
    'db' => [
        'driver' => 'pdo_mysql',
        'host' => '127.0.0.1',
        'dbname' => 'digi_test',
        'user' => 'digi_test_user',
        'password' => 'digi_test_pass'
    ],

];