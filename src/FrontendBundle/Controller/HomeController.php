<?php
namespace FrontendBundle\Controller;

session_start();
use Doctrine\ORM\EntityManager;
use MainBundle\Controller\BaseController;
use MainBundle\Services\CacheManager;
use MainBundle\Services\DBManager;
use ProductBundle\Entity\Product;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use MainBundle\Services\Template;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;

/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/14/17
 * Time: 7:29 PM
 */
class HomeController extends BaseController
{


    public function homeAction(Request $request)
    {
        $user = $this->getUser();
        return $this->renderView('Home/index.html.twig',['user' => $user]);
    }

    public function searchAction(Request $request)
    {
        $filters = $request->query->get('filters');
        $container = $this->getContainer();
        /** @var CacheManager $cacheManager */
        $cacheManager = $container->get('cache.manager');
        $results = $cacheManager->getCacheForQueryResult($filters);

        if ($results){
            $finalResults = json_decode($results);
            return new JsonResponse([
                'count' => count($finalResults),
                'results' => $finalResults
            ]);
        }
        $elasticSearchService = $container->get('elastic.search');
        $results = $elasticSearchService->searchProductsByFilter($filters);
        $sources = $results['hits']['hits'];
        $finalResults = [];
        foreach ($sources as $source){
            $finalResults[] =
                [
                    'id' => $source['_source']['id'],
                    'title' => $source['_source']['title'],
                    'description' => $source['_source']['description'],
                    'variants' => $source['_source']['variant']
                ];
        }
        $cacheManager->setCacheForQueryResult($filters,$finalResults,3600);

        return new JsonResponse([
            'count' => count($finalResults),
            'results' => $finalResults
            ]);

    }

    private function getDataFromDb($filters)
    {
        $container = $this->getContainer();
        /** @var DBManager $dbManager */
        $dbManager = $container->get('db.manager');
        $entityManager = $dbManager->getEntityManager();
        $productRepo = $entityManager->getRepository(Product::class);
        $products = $productRepo->searchProducts($filters);

        $finalResults = [];
        $productIds = [];
        foreach ($products as $product){
            if (!in_array($product['id'],$productIds)){
                $productIds []= $product['id'];
                $finalResults[]=[
                    'id' => $product['id'],
                    'title' => $product['title'],
                    'description' => $product['description'],
                    'variants' => [
                        [
                        'id' => $product['variantId'],
                        'color' => $product['color'],
                        'price' => $product['price']
                        ]
                    ]
                ];
            }else{
                foreach ($finalResults as &$result){
                    if ($result['id'] == $product['id']){
                        $result['variants'][]=[
                            'id' => $product['variantId'],
                            'color' => $product['color'],
                            'price' => $product['price']
                        ];
                    }
                }

            }
        }

        return $finalResults;
    }

    public function createIndexForEsAction()
    {
        $container = $this->getContainer();
        /** @var DBManager $dbManager */
        $dbManager = $container->get('db.manager');
        $elasticSearchService = $container->get('elastic.search');
        $elasticSearchClient = $elasticSearchService->getClient();
        $params = ['index' => 'digi_test'];
//        $response = $elasticSearchClient->indices()->delete($params);
//        die('here');
        $response = $elasticSearchClient->indices()->create($params);
        $params = [
            'index' => 'digi_test',
            'type' => 'product',
            'body' => [
                'product' => [
                    '_source' => [
                        'enabled' => true
                    ],
                    'properties' => [
                        'title' => [
                            'type' => 'string'
                        ],
                        'description' => [
                            'type' => 'string'
                        ],
                        'variant' => [
                            'type' => 'nested',
                            "properties" =>[
                                'color' => ['type' => 'string'],
                                'price' => ['type' =>'long'],
                                'id' => ['type' =>'short']
                            ]
                        ],
                    ]
                ]
            ]
        ];
        $elasticSearchClient->indices()->putMapping($params);
        die('done');
    }

}