<?php
namespace FrontendBundle\Controller;
session_start();

use MainBundle\Controller\BaseController;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use UserBundle\Entity\User;

class UserController extends BaseController
{

    public function registerAction(Request $request)
    {
        $email =$request->request->get('email');
        $password =$request->request->get('password');
        $confirmPassword =$request->request->get('confirm_password');
        if (!isset($email) || $email==''){
            return new JsonResponse([
                'status' => 'error',
                'message' => 'email must not be empty'
            ]);
        }
        if ($confirmPassword !== $password){
            return new JsonResponse([
                'status' => 'error',
                'message' => 'password and confirmation are not the same'
            ]);
        }

        $userList = array(
            'password' => $password,
            'email' => $email,
        );

        $container = $this->getContainer();
        $userAuthenticationService = $container->get('digi.test.authentication');
        $statusCode = $userAuthenticationService->createUser($userList);

        if ($statusCode == -1){
            return new JsonResponse([
                'status' => 'error',
                'message' => 'password must be at least 6 character'
            ]);
        } elseif ($statusCode == -2){
            return new JsonResponse([
                'status' => 'error',
                'message' => 'This email has already taken'
            ]);
        }


        return new JsonResponse([
            'status' => 'success',
            'message' => 'You are registered Successfully'
        ]);

    }


    public function loginPageAction()
    {
        return $this->renderView('User/login.html.twig');
    }

    public function loginAction(Request $request)
    {
        $email = $request->request->get('_username');
        $password = $request->request->get('_password');
        $container = $this->getContainer();
        $userAuthenticationService = $container->get('digi.test.authentication');
        $user = $userAuthenticationService->checkCredentialForUser($email,$password);
        if ($user != null){
            $_SESSION['userId'] = $user->getId();
            $_SESSION['type'] = 'FRONTEND';
            return new JsonResponse([
                'status' => 'success',
                'message' => ''
            ]);
        }else{
            return new JsonResponse([
                'status' => 'error',
                'message' => 'the credential not found'
            ]);
        }

    }

}