<?php

namespace BackendBundle\Controller;
use MainBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends BaseController
{

    public function loginPageAction()
    {
        return $this->renderView('Auth/login.html.twig');
    }

    public function loginAction(Request $request)
    {
        $email = $request->request->get('_username');
        $password = $request->request->get('_password');
        $container = $this->getContainer();
        $userAuthenticationService = $container->get('digi.test.authentication');
        $user = $userAuthenticationService->checkCredentialForUser($email,$password);
        if ($user != null){
            $_SESSION['userId'] = $user->getId();
            $_SESSION['type'] = 'BACKEND';
            return new JsonResponse([
                'status' => 'success',
                'message' => ''
            ]);
        }else{
            return new JsonResponse([
                'status' => 'error',
                'message' => 'the credential not found'
            ]);
        }
        
    }
}