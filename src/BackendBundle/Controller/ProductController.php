<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/14/17
 * Time: 11:57 PM
 */

namespace BackendBundle\Controller;


use Doctrine\ORM\EntityManager;
use MainBundle\Controller\BaseController;
use MainBundle\Services\DBManager;
use MainBundle\Services\Template;
use ProductBundle\Entity\Product;
use ProductBundle\Entity\ProductVariant;
use ProductBundle\Events\ProductCreateEvent;
use ProductBundle\Events\ProductDeletedEvent;
use ProductBundle\Events\ProductUpdateEvent;
use ProductBundle\Events\VariantAddedEvent;
use ProductBundle\Events\VariantDeletedEvent;
use ProductBundle\Events\VariantEditedEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends BaseController
{

    public function indexAction(Request $request)
    {
        return $this->renderView('Product/index.html.twig');
    }

    public function resultsAction(Request $request)
    {
        $em = $this->getEntityManager();
        $productRepo = $em->getRepository(Product::class);
        $products = $productRepo->getProductsByFilter();

        return new JsonResponse([
            'products' => $products
        ]);
    }

    public function newAction()
    {

        return $this->renderView('Product/new.html.twig');

    }

    public function createAction(Request $request)
    {
        $title = $request->request->get('title');
        $description = $request->request->get('description');
        $em = $this->getEntityManager();
        $product = new Product();
        $product->setTitle($title);
        $product->setDescription($description);
        $em->persist($product);
        $em->flush();
        $event = new ProductCreateEvent($product);
        $this->dispatchEvent(ProductCreateEvent::NAME,$event);
        $id = $product->getId();
        return $this->redirect('/admin/product/'.$id.'/edit');

    }

    public function editAction(Request $request,$id)
    {
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Product::class);
        $product = $repo->find($id);
        $productVariants = $product->getProductVariants();
        return $this->renderView('Product/edit.html.twig',['product' => $product, 'variants' => $productVariants]);
    }

    public function updateAction(Request $request,$id)
    {
        $title = $request->request->get('title');
        $description = $request->request->get('description');
        $em = $this->getEntityManager();
        $repo = $em->getRepository(Product::class);
        $product = $repo->find($id);
        if (isset($title)){
            $product->setTitle($title);
        }

        if (isset($title)){
            $product->setDescription($description);
        }
        $em->flush();
        $event = new ProductUpdateEvent($product);
        $this->dispatchEvent(ProductUpdateEvent::NAME,$event);
        return $this->redirect('/admin/product/'.$id.'/edit');

    }

    public function setVariantsAction(Request $request)
    {
        $variantData = $request->request->get('data');
        $productId = $request->request->get('productId');
        /** @var EntityManager $em */
        $em = $this->getEntityManager();
        $productRepo = $em->getRepository(Product::class);
        $variantRepo  = $em->getRepository(ProductVariant::class);
        $product = $productRepo->find($productId);
        if (!empty($variantData)){
            $em->beginTransaction();
            try{
                foreach ($variantData as $variantData){
                    if ($variantData['id'] == -1){
                        $variant = new ProductVariant();
                        $variant->setProduct($product);
                        $variant->setColor($variantData['color']);
                        $variant->setPrice($variantData['price']);
                        $em->persist($variant);
                        $em->flush();
                        $event = new VariantAddedEvent($variant);
                        $this->dispatchEvent(VariantAddedEvent::NAME,$event);
                    }else{
                        $variant = $variantRepo->find($variantData['id']);
                        $variant->setColor($variantData['color']);
                        $variant->setPrice($variantData['price']);
                        $em->flush();
                        $event = new VariantEditedEvent($variant);
                        $this->dispatchEvent(VariantEditedEvent::NAME,$event);
                    }

                }
                $em->flush();
                $em->commit();
                return new JsonResponse([
                    'status' =>true,
                    'message' => 'successfully Inserted'
                ]);
            }catch (\Exception $ex){
                $em->rollback();
                //todo here we shoud change data in elastic search
                return new JsonResponse([
                    'status' =>false,
                    'message' => 'somthing wrong with price input'
                ]);
            }

        }

    }

    public function deleteAction(Request $request)
    {
        $id = $request->request->get('id');
        $em = $this->getEntityManager();
        $productRepo = $em->getRepository(Product::class);
        $product = $productRepo->find($id);
        if ($product){
            $em->beginTransaction();
            try{

                $variants = $product->getProductVariants();
                foreach ($variants as $variant){
                    $em->remove($variant);
                }
                $em->remove($product);
                $event = new ProductDeletedEvent($product);
                $this->dispatchEvent(ProductDeletedEvent::NAME,$event);
                $em->flush();
                $em->commit();
                return new JsonResponse([
                    'status' => true,
                    'message' => ''
                ]);
            }catch(\Exception $ex){
                $em->rollback();
                return new JsonResponse([
                    'status' => false,
                    'message' => 'Error in removing'
                ]);
            }

        }else{

            return new JsonResponse([
                'status' => false,
                'message' => 'not found'
            ]);
        }
    }

    public function variantDeleteAction(Request $request)
    {
        $id = $request->request->get('id');
        $em = $this->getEntityManager();
        $productVariantRepo = $em->getRepository(ProductVariant::class);
        $productVariant = $productVariantRepo->find($id);
        if ($productVariant) {
            $em->remove($productVariant);
            $em->flush();
            $event = new VariantDeletedEvent($productVariant);
            $this->dispatchEvent(VariantDeletedEvent::NAME,$event);
            return new JsonResponse([
                'status' => true,
                'message' => ''
            ]);
        }else{
            return new JsonResponse([
                'status' => false,
                'message' => 'not found'
            ]);
        }

    }


}