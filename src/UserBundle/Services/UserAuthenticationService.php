<?php
namespace UserBundle\Services;

use Doctrine\ORM\EntityManager;
use MainBundle\Services\DBManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use UserBundle\Entity\User;

/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/18/17
 * Time: 9:49 AM
 */

class UserAuthenticationService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function createUser(array $userData)
    {
        $userRepo = $this->em->getRepository(User::class);

        if (strlen($userData['password']) < 6){
            return -1;
        }
        $results = $userRepo->findUserByThisEmail($userData['email']);
        if (count($results) > 0)
        {
            return -2;
        }

        $uniqueId = uniqid(rand());
        $hashedPassword  = sha1(md5($userData['password']));
        $user = new User();
        $user->setEmail($userData['email']);
        $user->setIsActive(true);
        $user->setUsername($uniqueId);
        $user->setPassword($hashedPassword);
        $this->em ->persist($user);
        $this->em ->flush();
        return 1;

    }

    public function checkCredentialForUser($email,$password)
    {
        $userRepo = $this->em->getRepository(User::class);
        try {
            /** @var User $user */
            $user = $userRepo->loadUserByUsername($email);
            if ($user){
                $hashedPassword = sha1(md5($password));
                if ($user->getPassword() == $hashedPassword){
                    return $user;
                }else{
                    return null;
                }
            }
        } catch(UsernameNotFoundException $e) {
            return null;
        }
    }

}