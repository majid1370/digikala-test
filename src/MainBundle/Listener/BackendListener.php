<?php
namespace MainBundle\Listener;

session_start();

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class BackendListener
{
    private $em;
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;

    }

    public function handle(Event $event)
    {
        /** @var Request $request */
        $request = $event->getRequest();
        $userId = $_SESSION['userId'];
        $type = $_SESSION['type'];

        $userRepo = $this->em->getRepository(User::class);
        //todo we can chacked later base on user role or type !!!!!
        if (!$userId || $type == "FRONTEND"){
            $path = $request->getPathInfo();
            if (preg_match("/^\/admin/",$path)){
                if (!preg_match("/^\/admin\/auth/",$path)){
                    $response = new RedirectResponse('/admin/auth');
                    $event->setResponse($response);
                }

            }
        }

    }

}