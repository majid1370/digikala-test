<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/19/17
 * Time: 4:34 PM
 */

namespace MainBundle\Listener;


use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ExceptionListener
{
    public function handle(Event $event)
    {

        /** @var Request $request */
        $request = $event->getRequest();

        $response = new RedirectResponse('/error');
        $event->setResponse($response);




    }

}