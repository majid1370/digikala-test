<?php
namespace MainBundle\Resolver;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/16/17
 * Time: 6:48 PM
 */
class myControllerResolver extends ControllerResolver
{
    private $container;
    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;

    }

    public function getController(Request $request)
    {
//        if (!$controller = $request->attributes->get('_controller')) {
//            if (null !== $this->logger) {
//                $this->logger->warning('Unable to look for the controller as the "_controller" parameter is missing.');
//            }
//
//            return false;
//        }
        $controller = $request->attributes->get('_controller');
        if (is_array($controller)) {
            return $controller;
        }

        if (is_object($controller)) {
            if (method_exists($controller, '__invoke')) {
                return $controller;
            }

            throw new \InvalidArgumentException(sprintf('Controller "%s" for URI "%s" is not callable.', get_class($controller), $request->getPathInfo()));
        }

        if (false === strpos($controller, ':')) {
            if (method_exists($controller, '__invoke')) {
                return $this->instantiateController($controller);
            } elseif (function_exists($controller)) {
                return $controller;
            }
        }

        $callable = $this->createController($controller);

        if (!is_callable($callable)) {
            throw new Exception('the controller is not callable');
//            throw new \InvalidArgumentException(sprintf('The controller for URI "%s" is not callable. %s', $request->getPathInfo(), $this->getControllerError($callable)));
        }

        return $callable;
    }

    public function instantiateController($controller)
    {
        return new $controller($this->container);

    }

}