<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/17/17
 * Time: 2:48 PM
 */

namespace MainBundle\Services;


use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class Security
{
    private $authenticationManager;
    private $authorizationChecker;
    private $tokenStorage;

    public function __construct(AuthenticationProviderManager $authenticationProviderManager,authorizationChecker $authorizationChecker,TokenStorage $tokenStorage)
    {
        $this->authenticationManager = $authenticationProviderManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;

    }

    public function getAuthenticationManager()
    {
        return $this->authenticationManager;
    }


    public function getAuthorizationChecker()
    {
        return $this->authorizationChecker;
    }
    
    public function getTokenStorage()
    {
        return $this->tokenStorage;
    }

}