<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/17/17
 * Time: 12:04 AM
 */

namespace MainBundle\Services;


use Elasticsearch\Client;

class ElasticSearch
{
    private $client;
    public function __construct(Client $client)
    {
        $this->client = $client;
        
    }

    public function getClient()
    {
        return $this->client;
    }

    public function searchProductsByFilter(array $filters)
    {
        $params = array();
        $params['index'] = 'digi_test';
        $params['type'] = 'product';
        $params['body'] = [];
        $params['size'] = 20;
        $params['from'] = 0;
        $query = [];
        $nested = [];
        if (isset($filters['title']) && $filters['title'] != ''){
            $title = $filters['title'];
            $queryString = [];
            $queryString['fields'] = ["title","description"];
            $queryString['query'] = "title: *".$title."* or description:*".$title.'*';
            $query['and'][]['query_string'] = $queryString;
        }

        if (isset($filters['lowPrice']) && $filters['lowPrice'] != ''){
            $nested['path'] = 'variant';
            $nested['query']['and'][]['range']['variant.price']['gte'] = $filters['lowPrice'];
            $query['and'][]['nested'] = $nested;


        }

        if (isset($filters['highPrice']) && $filters['highPrice'] != ''){
            $nested['path'] = 'variant';
            $nested['query']['and'][]['range']['variant.price']['lte'] = $filters['highPrice'];
            $query['and'][]['nested'] = $nested;
        }

        if (isset($filters['color']) && $filters['color'] != ''){
            $color = $filters['color'];
            $nested['path'] = 'variant';
            $nested['query']['and'][]['wildcard']['variant.color'] = "*" .$color . "*";
            $query['and'][]['nested'] = $nested;
        }
        
        if (isset($filters['page']) && $filters['page'] != ''){
            $params['from'] = $filters['page'] * $params['size'] = 20;
        }
        if (!empty($query)){
            $params['body']['query'] = $query;
        }
        return $this->getClient()->search($params);
    }

}