<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/24/17
 * Time: 2:22 PM
 */

namespace MainBundle\Services;


use Predis\Client;

class CacheManager
{
    private $cacheHandler;
    private $expire;

    public function __construct($cacheHandler, $expire =3600)
    {
        $this->cacheHandler = $cacheHandler;
        $this->expire = $expire;
    }

    //this is set out of construct so it could be changed in run time to other ones by calling set method
    public function setCacheHandler($cacheHandler)
    {
        $this->cacheHandler = $cacheHandler;
    }

    public function setCacheForQueryResult($filters,$results,$expireTime = null)
    {

        $key = json_encode($filters);
        $resultsToCache = json_encode($results);
        $expire = $expireTime ? $expireTime : $this->expire;
        $this->cacheQueryResult($key,$resultsToCache,$expire);
        return true;
    }

    public function getCacheForQueryResult($filters)
    {
        $key = json_encode($filters);
        $results = $this->cacheHandler->get($key);
        return $results;
    }

    private function cacheQueryResult($key,$value,$expire)
    {
        if ($this->cacheHandler instanceof Client){
            $this->cacheHandler->setex($key,$expire,$value);
        }elseif ($this->cacheHandler instanceof \Memcached){
            $stat = $this->cacheHandler->set($key,$value,$expire);
        }

        //todo must be handled base on result if query has cached or not
        return true;
    }


}