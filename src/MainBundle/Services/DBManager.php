<?php

namespace MainBundle\Services;

use Doctrine\ORM\EntityManager;

class DBManager
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }


}