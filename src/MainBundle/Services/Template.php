<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/15/17
 * Time: 11:01 PM
 */

namespace MainBundle\Services;


class Template
{

    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;

    }

    public function getTemplateEngine()
    {
        return $this->twig;
    }

}