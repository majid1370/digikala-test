<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/18/17
 * Time: 8:22 PM
 */

namespace MainBundle\Services;


use Symfony\Component\EventDispatcher\EventDispatcher;

class MyEventDispatcher
{
    private $eventDispatcher;
    public function __construct(EventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        
    }

    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }

}