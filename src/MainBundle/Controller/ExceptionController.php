<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/19/17
 * Time: 4:36 PM
 */

namespace MainBundle\Controller;


class ExceptionController  extends BaseController
{
    
    public function exceptionAction()
    {
        
        return $this->renderView('Error/error.html.twig');
        
    }

}