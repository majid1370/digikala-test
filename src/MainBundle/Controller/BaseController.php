<?php

namespace MainBundle\Controller;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/14/17
 * Time: 10:44 PM
 */
class BaseController
{
    private $container;
    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;
    }

    public function getContainer()
    {
        return $this->container;
    }

    protected function redirect($url)
    {

        return new RedirectResponse($url);


    }

    protected function renderView($path,array $params =[])
    {
        $template = $this->container->get('template');
        $twig = $template->getTemplateEngine();
        $view = $twig->render($path,$params);
        return new Response($view);
    }

    protected function getEntityManager()
    {
        $dBManager = $this->container->get('db.manager');
        $entityManager = $dBManager->getEntityManager();
        return $entityManager;

    }

    public function getUser()
    {
        $userId = $_SESSION['userId'];
        if ($userId){
            $em = $this->getEntityManager();
            $userRepo = $em->getRepository(User::class);
            $user = $userRepo->find($userId);
            return $user;

        }else{
            return null;
        }

//        if (!$this->container->has('security.token_storage')) {
//            throw new \LogicException('The SecurityBundle is not registered in your application.');
//        }
//
//        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
//            return;
//        }
//
//        if (!is_object($user = $token->getUser())) {
//            // e.g. anonymous authentication
//            return;
//        }

        return $user;
    }

    public function dispatchEvent($name,$event)
    {
        /** @var EventDispatcher $eventDispatcher */
        $eventDispatcher = $this->getContainer()->get('my.event.dispatcher')->getEventDispatcher();
        $eventDispatcher->dispatch($name,$event);
    }
}