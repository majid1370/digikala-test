<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/18/17
 * Time: 7:35 PM
 */

namespace ProductBundle\Listener;


use Elasticsearch\Client;
use MainBundle\Services\ElasticSearch;
use ProductBundle\Events\ProductCreateEvent;
use ProductBundle\Events\ProductDeletedEvent;
use ProductBundle\Events\ProductUpdateEvent;
use ProductBundle\Events\VariantAddedEvent;
use ProductBundle\Events\VariantDeletedEvent;
use ProductBundle\Events\VariantEditedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductChangeSubscriber implements EventSubscriberInterface
{
    private $elasticSearch;
    public function __construct(ElasticSearch $elasticSearch)
    {
        $this->elasticSearch = $elasticSearch;
    }

    public static function getSubscribedEvents()
    {
        return array(
            ProductCreateEvent::NAME => [
                ['processCreation',1]
            ],
            ProductUpdateEvent::NAME => [
                ['processUpdate',1]
            ],
            VariantAddedEvent::NAME => [
                ['processVariantAddition',1]
            ],
            VariantEditedEvent::NAME => [
                ['processVariantEdit',1]
            ],
            ProductDeletedEvent::NAME => [
                ['processProductDelete',1]
            ],
            VariantDeletedEvent::NAME => [
                ['processVariantDelete',1]
            ]
        );
    }

    public function processCreation(ProductCreateEvent $event)
    {
        $product = $event->getProduct();
        /** @var Client $es */
        $es = $this->elasticSearch->getClient();
        $es->index([
            'index' => 'digi_test',
            'type' => 'product',
            'id' => $product->getId(),
            'body' => [
                'id' => $product->getId(),
                'title' => $product->getTitle(),
                'description' => $product->getDescription(),
            ]
        ]);

    }

    public function processUpdate(ProductUpdateEvent $event)
    {
        $product = $event->getProduct();
        /** @var Client $es */
        $es = $this->elasticSearch->getClient();
        $params = [
            'index' => 'digi_test',
            'type' => 'product',
            'id' => $product->getId(),
        ];
        $result = $es ->get($params);
        $result['_source']['title'] = $product->getTitle();
        $result['_source']['description'] = $product->getDescription();

        $params['body']['doc'] = $result['_source'];
        $results = $es->update($params);

    }
    public function processVariantAddition(VariantAddedEvent $event)
    {
        $variant = $event->getVariant();
        $productId = $variant->getProduct()->getId();
        $es = $this->elasticSearch->getClient();
        $params = [
            'index' => 'digi_test',
            'type' => 'product',
            'id' => $productId,
        ];
        $result = $es ->get($params);
        $result['_source']['variant'][] = [
            'id' => $variant->getId(),
            'color' => $variant->getColor(),
            'price' => $variant->getPrice(),
        ];
        $params['body']['doc'] = $result['_source'];
        $results = $es->update($params);

    }

    public function processVariantEdit(VariantEditedEvent $event)
    {
        $variant = $event->getVariant();
        $product = $variant->getProduct();
        //todo maybe there would be a better method to update nested ones !!!!
        $productVariants = $product->getProductVariants();
        $productVariantsData = [];
        foreach ($productVariants as $variant){
            $productVariantsData[] =[
                'id' => $variant->getId(),
                'color' => $variant->getColor(),
                'price' => $variant->getPrice(),
            ];
        }

        $es = $this->elasticSearch->getClient();
        $params = [
            'index' => 'digi_test',
            'type' => 'product',
            'id' => $product->getId()
        ];
        $result = $es ->get($params);
        $result['_source']['variant'] = $productVariantsData;
        $params['body']['doc'] = $result['_source'];
        $results = $es->update($params);
    }

    public function processProductDelete(ProductDeletedEvent $event)
    {
        $product = $event->getProduct();
        $params = [
            'index' => 'digi_test',
            'type' => 'product',
            'id' => $product->getId()
        ];
        $es = $this->elasticSearch->getClient();
        $result = $es->delete($params);

    }
    
    public function processVariantDelete(VariantDeletedEvent $event)
    {
        $variant = $event->getVariant();
        $product = $variant->getProduct();
        //todo maybe there would be a better method to update nested ones !!!!
        $productVariants = $product->getProductVariants();
        $productVariantsData = [];
        foreach ($productVariants as $variant){
            $productVariantsData[] =[
                'id' => $variant->getId(),
                'color' => $variant->getColor(),
                'price' => $variant->getPrice(),
            ];
        }

        $es = $this->elasticSearch->getClient();
        $params = [
            'index' => 'digi_test',
            'type' => 'product',
            'id' => $product->getId()
        ];
        $result = $es ->get($params);
        $result['_source']['variant'] = $productVariantsData;
        $params['body']['doc'] = $result['_source'];
        $results = $es->update($params);
    }
}