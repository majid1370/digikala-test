<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/18/17
 * Time: 10:23 PM
 */

namespace ProductBundle\Events;


use ProductBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class ProductDeletedEvent extends Event
{
    const NAME = 'PRODUCT_DELETED';
    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

}