<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/18/17
 * Time: 7:32 PM
 */

namespace ProductBundle\Events;


use ProductBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class ProductUpdateEvent extends Event
{
    const NAME = 'PRODUCT_EDITED';
    public $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

}