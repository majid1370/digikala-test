<?php

namespace ProductBundle\Events;

use ProductBundle\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class ProductCreateEvent extends Event
{
    const NAME = 'PRODUCT_CREATED';
    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

}