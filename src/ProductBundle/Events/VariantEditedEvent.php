<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 11/18/17
 * Time: 9:46 PM
 */

namespace ProductBundle\Events;


use ProductBundle\Entity\ProductVariant;
use Symfony\Component\EventDispatcher\Event;

class VariantEditedEvent extends Event
{
    const NAME = 'VARIANT_EDITED';
    public $variant;
    public function __construct(ProductVariant $variant)
    {
        $this->variant = $variant;
    }

    public function getVariant()
    {
        return $this->variant;
    }


}