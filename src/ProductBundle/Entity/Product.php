<?php
namespace ProductBundle\Entity;


/**
 * @Entity(repositoryClass="ProductBundle\Entity\Repository\ProductRepository") @Table(name="products") */
class Product implements \JsonSerializable
{

    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $title;

    /** @Column(type="string") **/
    protected $description;

    /** @OneToMany(targetEntity="ProductBundle\Entity\ProductVariant", mappedBy="product")   */
    protected $productVariants;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
        ];
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productVariants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productVariant
     *
     * @param \ProductBundle\Entity\ProductVariant $productVariant
     *
     * @return Product
     */
    public function addProductVariant(\ProductBundle\Entity\ProductVariant $productVariant)
    {
        $this->productVariants[] = $productVariant;

        return $this;
    }

    /**
     * Remove productVariant
     *
     * @param \ProductBundle\Entity\ProductVariant $productVariant
     */
    public function removeProductVariant(\ProductBundle\Entity\ProductVariant $productVariant)
    {
        $this->productVariants->removeElement($productVariant);
    }

    /**
     * Get productVariants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductVariants()
    {
        return $this->productVariants;
    }
}
