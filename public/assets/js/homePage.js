homePage = {
    productsDiv:$('.product-container'),
    filters:{
        lowPrice:null,
        highPrice:null,
        color:null,
        title:null,
        page:null
    },
    init:function(){
        homePage.filterHandler();
        homePage.getData();
    },
    filterHandler : function () {
        $('.filters-section').on('change','#low-price-box',function () {
            homePage.filters.lowPrice = $(this).val();
            homePage.getData();
        });
        $('.filters-section').on('change','#high-price-box',function () {
            homePage.filters.highPrice = $(this).val();
            homePage.getData();
        });
        $('.filters-section').on('change','#color-box',function () {
            homePage.filters.color = $(this).val();
            homePage.getData();
        });
        $('.filters-section').on('change','#title-box',function () {
            homePage.filters.title = $(this).val();
            homePage.getData();
        });
    },
    getData : function () {
        $.ajax({
            url: '/search',
            type: 'GET',
            data: {
                filters: homePage.filters
            }
        }).done(function (data) {
            results = data.results;
            homePage.productsDiv.find('.product-div').remove();
            for (i in results){
                homePage.addProductToPage(results[i]);
            }

        }).fail(function () {

        }).always(function () {

        })
    },
    addProductToPage : function (_product) {
        div = homePage.productsDiv.find('.clonable-div').clone();
        div.removeClass('clonable-div');
        div.removeClass('hide');
        div.addClass('product-div');
        var variants= "<div style='display: inline-block;'>";
        if (_product['variants']){
            for(var i=0; i< _product['variants'].length; i++){
                variants += '<div style="width:75px;display: inline-block; padding: 5px;"><div style="display: inline-block;">'+_product['variants'][i]['color']+'</div><br><div style="border-top: 1px solid red">'+_product['variants'][i]['price']+'</div></div>'
            }

        }
        variants += "</div>";


        div.find('h6').html(_product.title);
        div.find('p').html(_product.description);
        div.find('div.variants-div').html(variants);
        homePage.productsDiv.append(div);
    }
}