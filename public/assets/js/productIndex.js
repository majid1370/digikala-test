productIndex = {

    productsTable:$('#products-table'),

    init:function () {
        productIndex.getData();

        $('#products-table').on('click','button.delete-product',function () {
                productId = $(this).attr('product-id');
                productIndex.deleteProduct(productId);
        })
    },

    getData:function () {
        $.ajax({
            url: '/admin/product/products-results',
            type: 'GET',
            data: {}
        }).done(function (data) {
                products = data['products'];
            productIndex.productsTable.find('.product-row').remove();
                for(i in products){
                    productIndex.addProductRow(products[i]);
                }


        }).fail(function () {

        }).always(function () {

        })
    },

    addProductRow:function (_product) {

        row = productIndex.productsTable.find('.clonable-row').clone();
        row.removeClass('clonable-row');
        row.removeClass('hide');
        row.addClass('product-row');
        editLink = '<span style="color: red;"><a target="_blank" href="/admin/product/' + _product.id + '/edit">'+_product.id+'</a>';
        actions = '<a target="_blank" href="/admin/product/' + _product.id + '/edit"><button type="button" class="btn btn-default btn-sm">'
            + '<span class="glyphicon glyphicon-pencil"></span>'
             + '</button></a><button type="button" class="delete-product btn btn-default btn-sm" product-id="' + _product.id + '">'
            + '<span class="glyphicon glyphicon-remove"></span></button>';
        row.find('td.productId').html(editLink);
        row.find('td.productTitle').html(_product.title);
        row.find('td.productDescription').html(_product.description);
        row.find('td.actions').html(actions);
        productIndex.productsTable.append(row);
    },

    deleteProduct:function(productId){
        $.ajax({
            url: '/admin/product/delete',
            type: 'POST',
            data: {
                'id' : productId
            }
        }).done(function (data) {
            if (data.status == true){
                location.reload();
            }else{
                alert(data.message);

            }



        }).fail(function () {

        }).always(function () {

        })
    }

}