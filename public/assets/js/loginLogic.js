loginLogic = {
    init:function () {
        $('#register-submit').click(function (e) {
            e.preventDefault();
            loginLogic.register();
        });

        $('#login-submit').click(function (e) {
            e.preventDefault();
            loginLogic.login();
        });
    },
    register:function () {
        $.ajax({
            url: '/user/register',
            type: 'POST',
            data: {
                email: $('#email').val(),
                password: $('#password').val(),
                confirm_password: $('#confirm-password').val()
            }
        }).done(function (data) {

            if (data.status == "success"){
                window.location = '/';
            }else{

                $('.message-box span.errorMessage').html(data.message);
                $('.message-box').removeClass('hide');
            }
            


        }).fail(function () {

        }).always(function () {

        })
    },

    login:function () {
        $.ajax({
            url: '/user/login',
            type: 'POST',
            data: {
                _username: $('#login-email').val(),
                _password: $('#login-password').val(),
            }
        }).done(function (data) {
            if (data.status == "success"){
                window.location = '/';
            }else{
                $('.message-box span.errorMessage').html(data.message);
                $('.message-box').removeClass('hide');
            }


        }).fail(function () {

        }).always(function () {

        })
    }
}