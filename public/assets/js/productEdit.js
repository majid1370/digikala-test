productEdit = {
    productId: null,
    variantData:[],
    init:function(){
        $('#update-variant').click(function () {
            productEdit.insertData();
        });
        $('#add-variant').click(function () {
            productEdit.addNewVariantRow();
        });

        $('.delete-variant').click(function () {
            variantId = $(this).attr('variant-id');
            productEdit.deleteVariant(variantId);
        });
    },

    insertData : function () {
        // var priceRows = $('#variant-entry-table tr.priceRow');
        $( "tr.priceRow" ).each(function( index ) {
            variantId = $(this).attr('variantId');
            variantPrice = $(this).find('td.variantPrice input').val();
            variantColor = $(this).find('td.variantColor input').val();
            productEdit.variantData.push({'id':variantId,'price':variantPrice,'color':variantColor});
        });
        $.ajax({
            url: '/admin/product/set-variant',
            type: 'POST',
            data: {
                data: productEdit.variantData,
                productId: $('.product-insert').attr('product-id')
            }
        }).done(function (data) {
            if (data.status == true){
                location.reload();
            }else{
                alert(data.message);
            }

        }).fail(function () {

        }).always(function () {

        })
    },
    addNewVariantRow:function () {
        row = $('#variant-entry-table').find('.clonable-row').clone();
        console.log(row);
        row.removeClass('clonable-row');
        row.removeClass('hide');
        row.addClass('priceRow');
        $('#variant-entry-table').append(row);
    },
    deleteVariant:function (variantId) {
        $.ajax({
            url: '/admin/product/variant/delete',
            type: 'POST',
            data: {
                'id' : variantId
            }
        }).done(function (data) {
            if (data.status == true){
                location.reload();
            }else{
                alert(data.message);

            }



        }).fail(function () {

        }).always(function () {

        })
    }
}