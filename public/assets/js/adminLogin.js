loginLogic = {
    init:function () {
        $('#login-submit').click(function (e) {
            e.preventDefault();
            loginLogic.login();
        });
    },
    login:function () {
        $.ajax({
            url: '/admin/auth/login',
            type: 'POST',
            data: {
                _username: $('#login-email').val(),
                _password: $('#login-password').val(),
            }
        }).done(function (data) {
            if (data.status == "success"){
                window.location = '/admin/product/index';
            }else{
                $('.message-box span.errorMessage').html(data.message);
                $('.message-box').removeClass('hide');
            }

        }).fail(function () {

        }).always(function () {

        })
    }
}