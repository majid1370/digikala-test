<?php

use Symfony\Component\HttpFoundation\Request;
$loader = require '../vendor/autoload.php';
require '../app/Kernel.php';
require '../app/config/parameters.php';


$request = Request::createFromGlobals();
$kernel = new Kernel();
$kernel->setRouteCollection();
$container  = $kernel->setContainer();
$container  = $kernel->setEvents();
$httpKernel = $kernel->getHttpKernel();
$response = $httpKernel->handle($request);
$response->send();